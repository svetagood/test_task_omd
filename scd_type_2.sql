SELECT post_id,
       views,
       likes,
       shares,
       MIN(dttm) as effective_from,
       LEAD(MIN(dttm), 1, '9999-12-31 23:59:59.000000') OVER (PARTITION BY post_id ORDER BY MAX(dttm)) as effective_to
FROM posts_stats
GROUP BY post_id, views, likes, shares
ORDER BY post_id, views;