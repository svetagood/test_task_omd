SELECT
a.post_id,
a.views,
a.likes,
a.shares,
MIN(effective_from) AS effective_from,
CASE WHEN effective_to IS NULL THEN '9999-12-31 23:59:59.000000' ELSE MIN(effective_to) END AS effective_to

FROM

(SELECT 
       post_id,
       views,
       likes,
       shares,
       dttm AS effective_from
FROM posts_stats) AS a

LEFT JOIN

(SELECT 
       post_id,
       views,
       likes,
       shares,
       dttm AS effective_to
FROM posts_stats) AS b

ON a.post_id = b.post_id AND (a.views <> b.views OR a.likes <> b.likes OR a.shares <> b.shares) AND effective_to > effective_from

GROUP BY 
a.post_id,
a.views,
a.likes,
a.shares

ORDER BY a.post_id, effective_from